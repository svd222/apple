<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 13.09.19
 * Time: 13:51
 */
namespace backend\components;

use backend\components\events\FruitEvent;
use backend\interfaces\ConditionInterface;
use backend\interfaces\FruitInterface;
use common\models\guides\FruitStateGuide;
use common\models\guides\FruitStatusGuide;
use yii\base\Component;
use yii\db\ActiveRecord;

/**
 * Class Fruit is the base class for all fruits: apple, strawwberry, e.t.c.
 * @package backend\components
 */
class Fruit extends Component implements FruitInterface
{
    const EVENT_FRUIT_CREATED = 'fruit_created';

    const EVENT_BEFORE_EAT = 'fruit_before_eat';

    const EVENT_AFTER_EAT = 'fruit_after_eat';

    const EVENT_BEFORE_HARVEST = 'fruit_before_harvest';

    const EVENT_AFTER_HARVEST = 'fruit_after_harvest';

    const EVENT_BEFORE_REMOVE = 'fruit_before_remove';

    const EVENT_AFTER_REMOVE = 'fruit_after_remove';

    /**
     * Base action for all possible public actions of this component, like as eat(), harvest(), remove() and and possibly others
     *
     * @param ConditionInterface $condition
     * @param $actionName
     * @return bool
     */
    public function action(ConditionInterface $condition, $actionName): bool
    {
        /**
         * @var ActiveRecord $model
         */
        $model = $condition->params['model'];

        $eventName = 'fruit_before_'.$actionName;
        $event = new FruitEvent();
        $event->model = $model;
        $this->trigger($eventName, $event);

        if ($condition->execute()) {
            $fullActionName = $actionName . 'Action';
            if ($this->$fullActionName($condition)) {
                $eventName = 'fruit_after_'.$actionName;
                $event = new FruitEvent();
                $event->model = $model;
                $this->trigger($eventName, $event);

                return true;
            }
        }
        return false;
    }

    /**
     * @param ConditionInterface $executeCondition
     * @return bool
     */
    public function harvest(ConditionInterface $executeCondition): bool
    {
        return $this->action($executeCondition, 'harvest');
    }

    protected function harvestAction(ConditionInterface $harvest): bool
    {
        /**
         * @var ActiveRecord $model
         */
        $model = $harvest->params['model'];
        $model->state = FruitStateGuide::STATE_HARVESTED;
        $model->status = FruitStatusGuide::STATUS_HARVESTED;
        $model->fall_time = time();
        if ($model->save()) {
            return true;
        }
        return false;
    }

    /**
     * @param ConditionInterface $executeCondition
     * @return bool
     */
    public function eat(ConditionInterface $executeCondition): bool
    {
        return $this->action($executeCondition, 'eat');
    }

    /**
     * @param ConditionInterface $eat
     * @return bool
     */
    protected function eatAction(ConditionInterface $eat): bool
    {
        /**
         * @var ActiveRecord $model
         */
        $model = $eat->params['model'];
        $size = $eat->params['size'];

        $model->size -= $size;
        if ($model->save()) {
            return true;
        }

        return false;
    }

    /**
     * @param ConditionInterface $executeCondition
     * @return bool
     */
    public function remove(ConditionInterface $executeCondition): bool
    {
        return $this->action($executeCondition, 'remove');
    }

    /**
     * @param ConditionInterface $remove
     * @return bool
     */
    protected function removeAction(ConditionInterface $remove): bool
    {
        /**
         * @var ActiveRecord $model
         */
        $model = $remove->params['model'];

        if ($model->delete()) {
            return true;
        }
        return false;
    }
}