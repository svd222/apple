<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 18.09.19
 * Time: 16:05
 */
namespace backend\components;

use backend\components\events\FruitEvent;
use backend\helpers\FruitColor;
use yii\base\Event;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\helpers\VarDumper;

class AppleFactory
{
    public static function create(): ActiveRecord
    {
        $color = FruitColor::generate();
        $className = \common\models\Apple::class;
        if (class_exists($className)) {
            /**
             * @var ActiveRecord $model
             */
            $model = new \common\models\Apple();
            $model->color = $color;
            if ($model->save()) {
                $event = new FruitEvent();
                $event->model = $model;
                Event::trigger(Fruit::class, Fruit::EVENT_FRUIT_CREATED, $event);
                return $model;
            } else {
                throw new Exception(VarDumper::dumpAsString($model->errors));
            }
        }
        return null;
    }
}