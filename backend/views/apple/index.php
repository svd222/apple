<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\assets\AppleIndexAsset;
use common\models\guides\FruitStateGuide;

/* @var $this yii\web\View */
/* @var $searchModel common\models\AppleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Apples');
$this->params['breadcrumbs'][] = $this->title;

AppleIndexAsset::register($this);
?>
<div class="apple-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
        foreach (Yii::$app->session->allFlashes as $key => $message) {
            ?>
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo Yii::$app->session->getFlash($key); ?>
            </div>
            <?php
        }
    ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Apples'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'label' => 'Eaten (%)',
                'content' => function($model, $key, $index) {
                    return 1 - $model->size;
                },
                'attribute' => 'size',
            ],
            'color',
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:d/m/Y H:i:s']
            ],
            [
                'class' => \yii\grid\DataColumn::class,
                'attribute' => 'state',
                'label' => 'State',
                'content' => function($model, $key, $index) {
                    /**
                     * @var \common\models\Apple $model
                     */
                    switch ($model->state) {
                        case FruitStateGuide::STATE_GROW : {
                            return 'On tree';
                            break;
                        }
                        case FruitStateGuide::STATE_HARVESTED : {
                            return 'Fell on the ground';
                            break;
                        }
                        case FruitStateGuide::STATE_ROTTEN : {
                            return 'Rotten';
                            break;
                        }
                    }
                },
                'filter' => [
                    FruitStateGuide::STATE_GROW => 'On tree',
                    FruitStateGuide::STATE_HARVESTED => 'Fell on the ground',
                    FruitStateGuide::STATE_ROTTEN => 'Rotten',
                ],
                'filterInputOptions' => ['prompt' => 'Choose state', 'class' => 'form-control', 'id' => null]
            ],
            [
                'attribute' => 'fall_time',
                'format' => ['date', 'php:d/m/Y H:i:s']
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '<div class="apple-actions">{harvest}</div><div class="apple-actions">{eat}</div>',
                'buttons' => [
                    'harvest' => function ($url, $model) {
                        $harvest = new \backend\components\conditions\Harvest([
                            'model' => $model
                        ]);
                        $canBeHarvested = true;
                        try {
                            $harvest->execute();
                        } catch (\yii\base\InvalidArgumentException $e) {
                            $canBeHarvested = false;
                        }
                        if ($canBeHarvested) {
                            return Html::a('<span class="glyphicon glyphicon-download-alt"></span>', $url, [
                                'title' => Yii::t('app', 'Fell to ground'),
                            ]);
                        }

                        return '';
                    },
                    'eat' => function ($url, $model) {
                        $eat = new \backend\components\conditions\Eat([
                            'model' => $model,
                            'size' => 0
                        ]);
                        $canBeEaten = true;
                        try {
                            $eat->execute();
                        } catch (\yii\base\InvalidArgumentException $e) {
                            $canBeEaten = false;
                        }
                        if ($canBeEaten) {
                            $form = Html::beginForm('/apple/eat/' . $model->id, 'post');

                            $input = Html::textInput('size',null,
                                [
                                    'class' => 'form-control',
                                    'maxlength' => 5,
                                    'size' => 5
                                ]);
                            $content = $input . ' ' .   '<span class="glyphicon glyphicon-apple apple-eat"></span>';

                            $content = '<div style="text-align:center">' . $content . '</div>';

                            $form .= $content;

                            $form .= Html::endForm();

                            return $form;
                        }

                        return '';
                    },
                ]
            ],
        ],
    ]); ?>


</div>
