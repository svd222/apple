<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,

    'number_of_rotten_at_a_time' => 1000,
    'fruitColors' => ['green', 'red', 'pink', 'white'],
    'rottingTime' => 5 * 3600,
];
