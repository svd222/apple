<?php

namespace backend\controllers;

use backend\components\AppleAction;
use backend\components\AppleFactory;
use backend\components\conditions\Eat;
use backend\components\conditions\Harvest;
use backend\components\conditions\Remove;
use Yii;
use common\models\Apple;
use common\models\AppleSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AppleController implements the 'eat', 'harvest', 'remove' actions for Apple model.
 */
class AppleController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    /**
     * Lists all Apple models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AppleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a set of Apple models.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $max = mt_rand(1, 8);
        for ($i = 0; $i < $max; $i++) {
            $model = AppleFactory::create();
        }

        Yii::$app->session->setFlash('fruits_created', $max . ' fruits has been created');
        return $this->redirect(['index']);
    }

    public function actions()
    {
        $actions = parent::actions();

        $actions['eat'] = [
            'class' => AppleAction::class,
            'executeCondition' => Eat::class,
            'requestedParams' => ['size']
        ];

        $actions['harvest'] = [
            'class' => AppleAction::class,
            'executeCondition' => Harvest::class,
            'requestedParams' => []
        ];

        $actions['remove'] = [
            'class' => AppleAction::class,
            'executeCondition' => Remove::class,
            'requestedParams' => []
        ];

        return $actions;
    }

    /**
     * Finds the Apple model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Apple the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id)
    {
        if (($model = Apple::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
