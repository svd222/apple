<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 18.09.19
 * Time: 16:05
 */
namespace backend\helpers;

use Yii;

class FruitColor
{
    public static function generate()
    {
        $colors = Yii::$app->params['fruitColors'];
        $count = count($colors);
        return $colors[mt_rand(0, $count - 1)];
    }
}