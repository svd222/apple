<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 13.09.19
 * Time: 14:16
 */
namespace backend\components\conditions;

use backend\interfaces\ConditionInterface;
use common\models\guides\FruitStateGuide;
use yii\base\InvalidArgumentException;
use yii\db\ActiveRecord;

class Harvest implements ConditionInterface
{
    public function __construct(array $params = null)
    {
        $this->params = $params;
    }

    public $params;

    public function execute(): bool
    {
        /**
         * @var ActiveRecord $model
         */
        $model = $this->params['model'];

        if (!$model) {
            throw new InvalidArgumentException("Instance not exists");
        }

        $className = basename(str_replace('\\', '/', get_class($model)));

        if ($model->state != FruitStateGuide::STATE_GROW) {
            throw new InvalidArgumentException("$className is still growing");
        }

        if ($model->state == FruitStateGuide::STATE_ROTTEN) {
            throw new InvalidArgumentException("$className is rotten");
        }

        if ($model->state == FruitStateGuide::STATE_HARVESTED) {
            throw new InvalidArgumentException("$className is already harvested");
        }

        return true;
    }
}