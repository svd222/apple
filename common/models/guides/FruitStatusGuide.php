<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 18.09.19
 * Time: 13:41
 */
namespace common\models\guides;

class FruitStatusGuide
{
    const STATUS_GROW = 0; //on tree (apple), in the ground (strawberry), e.t.c.

    const STATUS_HARVESTED = 1; //fallen down (apple), picked up (strawberry), e.t.c.
}