<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 13.09.19
 * Time: 13:54
 */
namespace backend\interfaces;

/**
 * Interface FruitInterface
 * @package backend\interfaces
 */
interface FruitInterface
{
    public function eat(ConditionInterface $executeCondition): bool;

    public function harvest(ConditionInterface $executeCondition): bool;

    public function remove(ConditionInterface $executeCondition): bool;
}