<?php

use yii\db\Migration;

/**
 * Class m190913_102205_create_table_apple
 */
class m190913_102205_create_table_apple extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%apple}}', [
            'id' => $this->primaryKey(),
            'size' => $this->float()->notNull()->defaultValue(1),
            'color' => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0)->comment('0 - On tree :, 1 - Fallen down)'),
            'state' => $this->smallInteger()->notNull()->defaultValue(0)->comment('0 - On tree :, 1 - Fallen down, 2 - Rotten)'),
            'fall_time' => $this->integer()->null(),
        ]);

        $this->createIndex('k_apple_size', '{{%apple}}', 'size');
        $this->createIndex('k_apple_color', '{{%apple}}', 'color');
        $this->createIndex('k_apple_status', '{{%apple}}', 'status');
        $this->createIndex('k_apple_state', '{{%apple}}', 'state');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%apple}}');
    }
}
