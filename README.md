# INSTALLATION #

``` $ sudo mkdir -m 0755 /var/www/apple && cd apple ```

``` $ git clone git@bitbucket.org:svd222/apple.git . ```

``` $ composer install ```

``` $ php init ```

Then create mysql database and configure db component in yii2

Don`t execute default yii2 migrations first!

Instead of executing default migrations you should execute [dektrium/yii2-user](https://github.com/dektrium/yii2-user/blob/master/docs/getting-started.md) : 

``` $ php yii migrate/up --migrationPath=@vendor/dektrium/yii2-user/migrations ```

Then create migrations from default folder

``` $ php yii migrate ```

Redis will also be required

## USAGE ##

After that go to backend main page and register (with any email), reload page and logging 
