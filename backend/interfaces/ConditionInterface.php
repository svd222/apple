<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 13.09.19
 * Time: 14:14
 */
namespace backend\interfaces;

interface ConditionInterface
{
    public function execute(): bool;
}