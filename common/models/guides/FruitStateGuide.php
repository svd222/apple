<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 18.09.19
 * Time: 13:44
 */
namespace common\models\guides;

class FruitStateGuide
{
    const STATE_GROW = 0; //on tree (apple), in the ground (strawberry), e.t.c.

    const STATE_HARVESTED = 1; //fallen down (apple), picked up (strawberry), e.t.c.

    const STATE_ROTTEN = 2; //Rotten
}