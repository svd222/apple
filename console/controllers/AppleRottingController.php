<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 18.09.19
 * Time: 22:29
 */
namespace console\controllers;

use yii\console\Controller;
use yii\helpers\ArrayHelper;

/**
 * Class AppleRottingController implements rot process
 *
 * usage:
 * ```php
 * $ cd /path/to/project
 * $ php yii apple-rotting/rot
 * ```
 *
 * @author svd <svd22286@gmail.com>
 * @since 2.0
 * @package console\controllers
 */
class AppleRottingController extends Controller
{
    public function actionRot()
    {
        $redis = \Yii::$app->redis;

        $rotten = $redis->executeCommand('lrange', ['rotting_time_and_id', 0, \Yii::$app->params['number_of_rotten_at_a_time']]);
        $rotten = array_map(function($value) use ($redis) {
            $timestamp = substr($value, 0, 10);
            if ($timestamp < time()) {
                $id = substr($value, 10);
                $redis->executeCommand('lrem', ['rotting_time_and_id', 0, $value])->execute();
                return [$timestamp, $id];
            }
            return [0, 0];
            //now value should be [time, id]
        }, $rotten);

        $rottenIds = ArrayHelper::getColumn($rotten, 1);

        $rotten = \Yii::$app->db->createCommand()->update('{{%apple}}', ['state' => 2], ['id' => $rottenIds, 'state' => 1, ])->execute();

        \Yii::warning($rotten .' apples has been rotten');
    }
}