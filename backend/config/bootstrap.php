<?php
\yii\base\Event::on(\backend\components\Fruit::class, \backend\components\Fruit::EVENT_BEFORE_EAT, function (\backend\components\events\FruitEvent $event) {
    Yii::info(\backend\components\Fruit::EVENT_BEFORE_EAT . ' triggered. ' . \yii\helpers\VarDumper::dumpAsString($event->model));
});

\yii\base\Event::on(\backend\components\Fruit::class, \backend\components\Fruit::EVENT_BEFORE_HARVEST, function (\backend\components\events\FruitEvent $event) {
    Yii::info(\backend\components\Fruit::EVENT_BEFORE_HARVEST . ' triggered. ' . \yii\helpers\VarDumper::dumpAsString($event->model));
});

\yii\base\Event::on(\backend\components\Fruit::class, \backend\components\Fruit::EVENT_BEFORE_REMOVE, function (\backend\components\events\FruitEvent $event) {
    Yii::info(\backend\components\Fruit::EVENT_BEFORE_REMOVE . ' triggered. ' . \yii\helpers\VarDumper::dumpAsString($event->model));
});

\yii\base\Event::on(\backend\components\Fruit::class, \backend\components\Fruit::EVENT_AFTER_EAT, function (\backend\components\events\FruitEvent $event) {
    Yii::info(\backend\components\Fruit::EVENT_AFTER_EAT . ' triggered. ' . \yii\helpers\VarDumper::dumpAsString($event->model));

    /**
     * @var \yii\db\ActiveRecord $model
     */
    $model = $event->model;
    if ($model->size <= 0) {
        $apple = new \backend\components\Apple();
        $removeCondition = new \backend\components\conditions\Remove([
            'model' => $model
        ]);

        if ($apple->remove($removeCondition)) {
            Yii::info('Fruit with id #' . $model->id . ' has been eaten and removed');
        }
    }
});

\yii\base\Event::on(\backend\components\Fruit::class, \backend\components\Fruit::EVENT_AFTER_HARVEST, function (\backend\components\events\FruitEvent $event) {
    Yii::info(\backend\components\Fruit::EVENT_AFTER_HARVEST . ' triggered. ' . \yii\helpers\VarDumper::dumpAsString($event->model));

    $redis = Yii::$app->redis;
    /**
     * @var \common\models\Apple $model
     */
    $model = $event->model;
    $rottingTime = Yii::$app->params['rottingTime'];
    $value = $model->fall_time + $rottingTime;
    $value .= $model->id;
    $redis->executeCommand('lpush', ['rotting_time_and_id', $value]);

    Yii::warning('Fruit fallen at: ' . date('Y-m-d H:i:s', $model->fall_time) . ' gone to rotten: ' . date('Y-m-d H:i:s', ($model->fall_time + $rottingTime)));
});

\yii\base\Event::on(\backend\components\Fruit::class, \backend\components\Fruit::EVENT_AFTER_REMOVE, function (\backend\components\events\FruitEvent $event) {
    Yii::info(\backend\components\Fruit::EVENT_AFTER_REMOVE . ' triggered. ' . \yii\helpers\VarDumper::dumpAsString($event->model));
});

\yii\base\Event::on(\backend\components\Fruit::class, \backend\components\Fruit::EVENT_FRUIT_CREATED, function (\backend\components\events\FruitEvent $event) {
    Yii::info(\backend\components\Fruit::EVENT_AFTER_REMOVE . ' triggered. ' . \yii\helpers\VarDumper::dumpAsString($event->model));
});