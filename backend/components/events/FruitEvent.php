<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 18.09.19
 * Time: 13:57
 */
namespace backend\components\events;

use yii\base\Event;
use yii\db\ActiveRecord;

class FruitEvent extends Event
{
    /**
     * @var ActiveRecord
     */
    public $model;
}